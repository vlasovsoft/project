#include "mainwindow.h"

#include <QApplication>
#include <QSettings>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setOrganizationName("vlasovsoft");
    a.setApplicationName("project");

    MainWindow w;
    w.show();

    return a.exec();
}
