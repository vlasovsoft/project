#include "taskdlg.h"
#include "ui_taskdlg.h"

TaskDlg::TaskDlg(QWidget *parent, Task& t)
    : QDialog(parent)
    , ui(new Ui::TaskDlg)
    , task(t)
{
    ui->setupUi(this);

    ui->leTitle->setText(task.getTitle());
    ui->spbDuration->setValue(task.getDuration());
    ui->txtDescription->setPlainText(task.getDescription());
}

TaskDlg::~TaskDlg()
{
    delete ui;
}

void TaskDlg::on_buttonBox_accepted()
{
    task.setTitle(ui->leTitle->text());
    task.setDuration(ui->spbDuration->value());
    task.setDescription(ui->txtDescription->toPlainText());
}
