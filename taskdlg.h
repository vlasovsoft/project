#ifndef TASKDLG_H
#define TASKDLG_H

#include <QDialog>

#include "task.h"

namespace Ui {
class TaskDlg;
}

class TaskDlg : public QDialog
{
    Q_OBJECT

public:
    explicit TaskDlg(QWidget *parent, Task& t);
    ~TaskDlg();

private slots:
    void on_buttonBox_accepted();

private:
    Ui::TaskDlg *ui;
    Task& task;
};

#endif // TASKDLG_H
