#ifndef PROJECTMODEL_H
#define PROJECTMODEL_H

#include <QAbstractItemModel>

#include "task.h"

class ProjectModel : public QAbstractItemModel
{
    Task* root;
    bool modified;

public:
    ProjectModel(QObject *parent = nullptr);
    ~ProjectModel() override;

    QVariant data(const QModelIndex &index, int role) const override;
    Qt::ItemFlags flags(const QModelIndex &index) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex &index) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    QModelIndex addTask(const QModelIndex&, const Task&);
    void removeTask(const QModelIndex&);
    bool indentTask(const QModelIndex&);
    bool outdentTask(const QModelIndex&);
    void moveTaskUp(const QModelIndex&);
    void moveTaskDown(const QModelIndex&);
    void editTask(const QModelIndex&, const Task&);
    void markTaskDone(const QModelIndex&);
    void saveToFile( const QString& fileName );
    bool loadFromFile( const QString& fileName );
    bool isModified() const { return modified; }
    void setModified( bool val ) { modified = val; }
};

#endif // PROJECTMODEL_H
