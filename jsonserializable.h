#ifndef JSONSERIALIZABLE_H
#define JSONSERIALIZABLE_H

class QJsonObject;

class JSonSerializable
{
public:
    virtual ~JSonSerializable();
    virtual void read(const QJsonObject&) = 0;
    virtual void write(QJsonObject&) const = 0;
};

#endif // JSONSERIALIZABLE_H
