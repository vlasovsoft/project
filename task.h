#ifndef TASK_H
#define TASK_H

#include <QString>
#include <QDateTime>
#include <QVector>
#include <QVariant>

#include "jsonserializable.h"

class Task: public JSonSerializable
{    
    friend class ProjectModel;

    typedef QVector<Task*> Tasks;

    Task* parent;
    Tasks children;

    QString title;
    QString description;
    int duration; // total time for this task
    int remain;   // remain time for this task

public:
    Task(Task* p);
    ~Task();

    QString getTitle() const {
        return title;
    }
    void setTitle( const QString& val ) {
        title = val;
    }
    QString getDescription() const {
        return description;
    }
    void setDescription( const QString& val ) {
        description = val;
    }
    int getDuration() const {
        return duration;
    }
    void setDuration( int val ) {
        duration = remain = val;
    }
    bool isDone() const {
        return 0 == remain;
    }
    void setDone( bool val ) {
        remain = val? 0 : duration;
    }

    void appendChild( Task* task );
    void insertChild( Task* task, int row );
    void removeChild( int row, bool del = true );

    Task* child(int row);
    int childCount() const;
    int columnCount() const;
    QVariant data(int column) const;
    int row() const;
    Task* parentTask();

    void update();

    void read(const QJsonObject& obj);
    void write(QJsonObject& obj) const;
};

#endif // TASK_H
