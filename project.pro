#-------------------------------------------------
#
# Project created by QtCreator 2019-03-04T11:23:55
#
#-------------------------------------------------

QT       += core gui

QT += widgets

TARGET = project
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

CONFIG += c++11

SOURCES += \
        main.cpp \
        mainwindow.cpp \
        task.cpp \
        projectmodel.cpp \
        taskdlg.cpp \
        jsonserializable.cpp
        
HEADERS += \
        mainwindow.h \
        task.h \
        projectmodel.h \
        taskdlg.h \
        jsonserializable.h

FORMS += \
        mainwindow.ui \
        taskdlg.ui

RESOURCES += images/images.qrc

