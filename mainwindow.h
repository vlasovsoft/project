#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "projectmodel.h"

class QSettings;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

    QString fileName;

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow() override;

protected:
    void closeEvent(QCloseEvent*) override;
    void saveViewState(QSettings& s) const;
    void restoreViewState(QSettings& s);
    void setCurrentFile(const QString& fn);
    QString strippedName(const QString &fullName) const;
    void openFile(const QString& fn);
    bool checkModified();
    void editTask(const QModelIndex&);

private slots:
    void on_actionNew_Task_triggered();
    void on_actionDelete_Task_triggered();
    bool on_actionSave_triggered();
    bool on_actionSave_As_triggered();
    void on_actionOpen_triggered();
    void on_actionNew_triggered();
    void on_actionIndent_triggered();
    void on_actionOutdent_triggered();
    void on_actionEdit_triggered();
    void on_actionUp_triggered();
    void on_actionDown_triggered();
    void on_actionDone_triggered();
    void on_actionExit_triggered();
    void openRecentFile();
    void activated(const QModelIndex&);

private:
    Ui::MainWindow *ui;
    ProjectModel* model;
};

#endif // MAINWINDOW_H
