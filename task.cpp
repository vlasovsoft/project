#include <QJsonObject>
#include <QJsonValue>
#include <QJsonArray>

#include "task.h"

Task::Task(Task* p)
    : parent(p)
    , duration(1) // default duration
    , remain(1)   // default duration
{
}

Task::~Task()
{
    qDeleteAll(children);
}

void Task::appendChild(Task* task)
{
    children.push_back(task);
    task->parent = this;
}

void Task::insertChild(Task *task, int row)
{
    children.insert(row, task);
    task->parent = this;
}

void Task::removeChild(int row, bool del)
{
    if ( row >= 0 && row < childCount() )
    {
        Task* task = children[row];
        children.remove(row);
        task->parent = nullptr;
        if ( del )
            delete task;
    }
}

Task* Task::child(int row)
{
    return children.value(row);
}

int Task::childCount() const
{
    return children.count();
}

int Task::columnCount() const
{
    return 2;
}

QVariant Task::data(int column) const
{
    switch ( column ) {
    case 0:
        return title;
    case 1:
        return remain == duration ?
            QVariant(duration) :
            QVariant(QString("%1/%2").arg(remain).arg(duration));
    }
    return QVariant();
}

int Task::row() const
{
    return parent != nullptr ? parent->children.indexOf(const_cast<Task*>(this)) : 0;
}

Task* Task::parentTask()
{
    return parent;
}

void Task::update()
{
    if ( childCount() > 0 )
    {
        int resultDuration = 0;
        int resultRemain = 0;
        for ( Task* child: children )
        {
            child->update();
            resultDuration += child->duration;
            resultRemain += child->remain;
        }
        duration = resultDuration;
        remain = resultRemain;
    }
}

void Task::read(const QJsonObject &obj)
{
    title = obj["title"].toString();
    duration = obj["duration"].toInt();
    remain = obj["remain"].toInt();
    description = obj["description"].toString();
    if ( obj.contains("children") )
    {
        QJsonArray array = obj["children"].toArray();
        for (QJsonValue val: array)
        {
            QJsonObject child = val.toObject();
            Task* task = new Task(nullptr);
            task->read(child);
            appendChild(task);
        }
    }
}

void Task::write(QJsonObject &obj) const
{
    obj["title"] = title;
    obj["duration"] = duration;
    obj["remain"] = remain;
    obj["description"] = description;
    if ( !children.isEmpty() )
    {
        QJsonArray array;
        for ( const Task* task: children )
        {
            QJsonObject child;
            task->write(child);
            array.push_back(child);
        }
        if ( !array.isEmpty() )
        {
            obj["children"] = array;
        }
    }
}
