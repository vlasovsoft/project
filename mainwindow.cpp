#include <QDebug>
#include <QCloseEvent>
#include <QSettings>
#include <QFileDialog>
#include <QStyledItemDelegate>
#include <QMessageBox>

#include "taskdlg.h"

#include "mainwindow.h"
#include "ui_mainwindow.h"

#define FILE_EXT ".proj"

class MyDelegate: public QStyledItemDelegate
{
public:
    MyDelegate( QObject* parent );
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
};

MyDelegate::MyDelegate( QObject* parent )
    : QStyledItemDelegate(parent)
{}

void MyDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    QStyleOptionViewItem opt = option;
    initStyleOption(&opt, index);
    if ( index.isValid() && 0 == index.column() ) {
        Task* task = static_cast<Task*>(index.internalPointer());
        if ( task->isDone() ) {
            opt.font.setStrikeOut(true);
        }
    }
    QStyledItemDelegate::paint(painter, opt, index);
}

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , model(new ProjectModel(this))
{
    ui->setupUi(this);
    ui->treeView->setSelectionMode(QAbstractItemView::NoSelection);
    ui->treeView->setModel(model);
    ui->treeView->setItemDelegate(new MyDelegate(ui->treeView));

    QSettings settings;
    restoreGeometry(settings.value("geometry").toByteArray());
    restoreState(settings.value("state").toByteArray());
    restoreViewState(settings);

    connect(ui->recentFile1, SIGNAL(triggered()), this, SLOT(openRecentFile()));
    connect(ui->recentFile2, SIGNAL(triggered()), this, SLOT(openRecentFile()));
    connect(ui->recentFile3, SIGNAL(triggered()), this, SLOT(openRecentFile()));
    connect(ui->recentFile4, SIGNAL(triggered()), this, SLOT(openRecentFile()));
    connect(ui->recentFile5, SIGNAL(triggered()), this, SLOT(openRecentFile()));

    connect(ui->treeView, SIGNAL(activated(QModelIndex)), this, SLOT(activated(QModelIndex)));

    setCurrentFile(fileName);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::closeEvent(QCloseEvent* e)
{
    QSettings settings;
    settings.setValue("geometry", saveGeometry());
    settings.setValue("state", saveState());
    saveViewState(settings);
    if ( checkModified() ) {
        e->accept();
    } else {
        e->ignore();
    }
}

void MainWindow::saveViewState(QSettings& s) const
{
    s.setValue("column_size", ui->treeView->columnWidth(0));
}

void MainWindow::restoreViewState(QSettings& s)
{
    ui->treeView->header()->setStretchLastSection(true);
    if ( s.contains("column_size") )
    {
        ui->treeView->setColumnWidth(0,s.value("column_size").toInt());
    }
}

void MainWindow::setCurrentFile(const QString &fn)
{
    fileName = fn;

    // update recent files
    QSettings settings;
    QStringList files = settings.value("recent_files").toStringList();
    files.removeAll("");
    files.removeAll(fileName);
    if ( !fileName.isEmpty() )
        files.prepend(fileName);
    while (files.size() > 5)
        files.removeLast();
    for (int i = 0; i < 5; ++i)
    {
        QAction* a = findChild<QAction*>(QString("recentFile%1").arg(i+1));
        if ( a != nullptr ) {
            if ( i < files.size() && QFileInfo(files[i]).isFile() ) {
                a->setText(tr("&%1 %2").arg(i + 1).arg(strippedName(files[i])));
                a->setData(files[i]);
                a->setVisible(true);
            } else {
                a->setText("");
                a->setData("");
                a->setVisible(false);
            }
        }
    }
    settings.setValue("recent_files", files);

    setWindowFilePath(fileName);
}

QString MainWindow::strippedName(const QString& fullName) const
{
    return QFileInfo(fullName).fileName();
}

void MainWindow::openFile(const QString& fn)
{
    ProjectModel* m = new ProjectModel(this);
    if ( m->loadFromFile(fn) ) {
        delete model;
        model = m;
        ui->treeView->setModel(model);
        ui->treeView->expandAll();
        setCurrentFile(fn);
    } else {
        delete m;
    }
}

bool MainWindow::checkModified()
{
    if ( model->isModified() )
    {
        int result = QMessageBox::question(this, tr("Save"), tr("Save changes?"), QMessageBox::Yes|QMessageBox::No|QMessageBox::Cancel );
        if ( QMessageBox::Yes == result ) {
            return on_actionSave_triggered();
        }
        else
        {
            return result != QMessageBox::Cancel;
        }
    }
    return true;
}

void MainWindow::editTask(const QModelIndex& idx)
{
    if ( idx.isValid() )
    {
        Task* task = static_cast<Task*>(idx.internalPointer());
        Task t = *task;
        TaskDlg dlg(this, t);
        if ( dlg.exec() ) {
            model->editTask(idx, t);
        }
    }
}

void MainWindow::on_actionNew_Task_triggered()
{
    Task task(nullptr);
    TaskDlg dlg(this, task);
    if ( dlg.exec() )
    {
        ui->treeView->setCurrentIndex(model->addTask( ui->treeView->currentIndex(), task ));
    }
}

void MainWindow::on_actionDelete_Task_triggered()
{
    model->removeTask( ui->treeView->currentIndex() );
}

bool MainWindow::on_actionSave_triggered()
{
    if ( fileName.isEmpty() )
    {
        return on_actionSave_As_triggered();
    }
    else
    {
        model->saveToFile(fileName);
    }
    return true;
}

bool MainWindow::on_actionSave_As_triggered()
{
    QString fn = QFileDialog::getSaveFileName(this,
        tr("Save Project"), "",
        tr("Project (*%1);;All Files (*)").arg(FILE_EXT));
    if ( !fn.isEmpty() )
    {
        if ( !fn.endsWith(FILE_EXT)) {
            fn.append(FILE_EXT);
        }
        model->saveToFile(fn);
        setCurrentFile(fn);
        return true;
    }
    return false;
}

void MainWindow::on_actionOpen_triggered()
{
    QString fn = QFileDialog::getOpenFileName(this, tr("Open Project"),
                                                    QString(),
                                                    tr("Project (*%1)").arg(FILE_EXT));
    if ( !fn.isEmpty() && fn != fileName )
    {
        if ( checkModified() ) {
            openFile(fn);
        }
    }
}

void MainWindow::on_actionNew_triggered()
{
    if ( checkModified() ) {
        ProjectModel* m = new ProjectModel(this);
        delete model;
        model = m;
        ui->treeView->setModel(model);
        fileName.clear();
    }
}

void MainWindow::on_actionIndent_triggered()
{
    if ( model->indentTask(ui->treeView->currentIndex()) )
    {
        ui->treeView->expand(model->parent(ui->treeView->currentIndex()));
    }
}

void MainWindow::on_actionOutdent_triggered()
{
    model->outdentTask(ui->treeView->currentIndex());
}

void MainWindow::on_actionEdit_triggered()
{
    editTask(ui->treeView->currentIndex());
}

void MainWindow::on_actionUp_triggered()
{
    model->moveTaskUp(ui->treeView->currentIndex());
}

void MainWindow::on_actionDown_triggered()
{
    model->moveTaskDown(ui->treeView->currentIndex());
}

void MainWindow::on_actionDone_triggered()
{
    model->markTaskDone(ui->treeView->currentIndex());
}

void MainWindow::on_actionExit_triggered()
{
    close();
}

void MainWindow::openRecentFile()
{
    QAction *action = qobject_cast<QAction *>(sender());
    if (action)
    {
        QString fn = action->data().toString();
        if ( !fn.isEmpty() && fn != fileName && checkModified() )
            openFile(fn);
    }
}

void MainWindow::activated(const QModelIndex& idx)
{
    editTask(idx);
}
