#include <QFile>
#include <QFont>
#include <QJsonObject>
#include <QJsonDocument>

#include "projectmodel.h"

ProjectModel::ProjectModel(QObject *parent)
    : QAbstractItemModel(parent)
    , root(new Task(nullptr))
    , modified(false)
{
}

ProjectModel::~ProjectModel()
{
    delete root;
}

QVariant ProjectModel::data(const QModelIndex& index, int role) const
{
    if (!index.isValid())
        return QVariant();

    Task* item = static_cast<Task*>(index.internalPointer());
    switch( role )
    {
    case Qt::DisplayRole:
        return item->data(index.column());
    }

    return QVariant();
}

Qt::ItemFlags ProjectModel::flags(const QModelIndex &index) const
{
    return index.isValid() ? QAbstractItemModel::flags(index) : Qt::NoItemFlags;
}

QVariant ProjectModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
    {
        switch( section )
        {
        case 0:
            return tr("Title");
        case 1:
            return tr("Duration");
        }
    }
    return QVariant();
}

QModelIndex ProjectModel::index(int row, int column, const QModelIndex &parent) const
{
    if (!hasIndex(row, column, parent))
        return QModelIndex();
    Task* parentTask = parent.isValid()?
        static_cast<Task*>(parent.internalPointer()) :
        root;
    Task* childTask = parentTask->child(row);
    return childTask != nullptr ?
        createIndex(row, column, childTask) :
        QModelIndex();
}

QModelIndex ProjectModel::parent(const QModelIndex &index) const
{
    if (!index.isValid())
        return QModelIndex();

    Task* childTask = static_cast<Task*>(index.internalPointer());
    Task* parentTask = childTask->parentTask();

    return parentTask != root ?
        createIndex(parentTask->row(), 0, parentTask) :
                QModelIndex();
}

int ProjectModel::rowCount(const QModelIndex &parent) const
{
    if (parent.column() > 0)
        return 0;

    Task* parentTask = parent.isValid() ?
        static_cast<Task*>(parent.internalPointer()) :
        root;

    return parentTask->childCount();
}

int ProjectModel::columnCount(const QModelIndex &parent) const
{
    return parent.isValid() ?
        static_cast<Task*>(parent.internalPointer())->columnCount() :
        root->columnCount();
}

QModelIndex ProjectModel::addTask(const QModelIndex& idx, const Task& task)
{
    QModelIndex pi(parent(idx));
    int row = 0;
    if ( idx.isValid() )
    {
        const Task* t = static_cast<const Task*>(idx.internalPointer());
        row = t->row() + 1;
    }
    beginInsertRows(pi, row, row);
    Task* pt = pi.isValid()?
        static_cast<Task*>(pi.internalPointer()) :
        root;
    Task* nt = new Task(pt);
    nt->setTitle(task.getTitle());
    nt->setDuration(task.getDuration());
    nt->setDescription(task.getDescription());
    pt->insertChild(nt, row);
    root->update();
    endInsertRows();
    emit dataChanged(QModelIndex(), QModelIndex());
    modified = true;
    return index(row, 0, pi);
}

void ProjectModel::removeTask(const QModelIndex &index)
{
    if ( index.isValid() )
    {
        QModelIndex parentIndex(parent(index));
        Task* task = static_cast<Task*>(index.internalPointer());
        int row = task->row();
        if ( row >= 0 )
        {
            beginRemoveRows(parentIndex, row, row);
            Task* pt = task->parentTask();
            pt->removeChild(row);
            if ( 0 == pt->childCount() ) {
                pt->setDuration(0);
            }
            root->update();
            endRemoveRows();
            emit dataChanged(QModelIndex(), QModelIndex());
            modified = true;
        }
    }
}

bool ProjectModel::indentTask(const QModelIndex& idx)
{
    bool result = false;
    if ( idx.isValid() )
    {
        // current task
        Task* task = static_cast<Task*>(idx.internalPointer());
        // source row
        int srow = task->row();
        if ( srow > 0 )
        {
            QModelIndex pi = parent(idx);
            // parent task
            Task* pt = pi.isValid()?
                static_cast<Task*>(pi.internalPointer()) :
                root;
            // sublign task (new parent)
            Task* st = pt->child(srow-1);
            // sublign index
            QModelIndex si( index( srow-1, 0, pi ) );
            // destination row
            int drow = st->childCount();
            // MOVE
            beginMoveRows(pi, srow, srow, si, drow);
            pt->removeChild(srow, false);
            st->appendChild(task);
            root->update();
            endMoveRows();
            emit dataChanged(QModelIndex(), QModelIndex());
            modified = true;
            result = true;
        }
    }
    return result;
}

bool ProjectModel::outdentTask(const QModelIndex &idx)
{
    bool result = false;
    if ( idx.isValid() )
    {
        // current task
        Task* task = static_cast<Task*>(idx.internalPointer());
        // parent index
        QModelIndex pi(parent(idx));
        if ( pi.isValid() )
        {
            // parent task
            Task* pt = static_cast<Task*>(pi.internalPointer());
            // grandparent index
            QModelIndex ppi(parent(pi));
            // grandparent task
            Task* ppt = ppi.isValid() ? static_cast<Task*>(ppi.internalPointer()) :
                                        root;
            // source row
            int srow = task->row();
            // destination row
            int drow = pt->row() + 1;
            // MOVE
            beginMoveRows(pi, srow, srow, ppi, drow);
            pt->removeChild(srow, false);
            if ( 0 == pt->childCount() )
            {
                pt->setDuration(0);
            }
            ppt->insertChild(task, drow);
            root->update();
            endMoveRows();
            emit dataChanged(QModelIndex(), QModelIndex());
            modified = true;
            result = true;            
        }
    }
    return result;
}

void ProjectModel::moveTaskUp(const QModelIndex &idx)
{
    if ( idx.isValid() )
    {
        // current task
        Task* task = static_cast<Task*>(idx.internalPointer());
        // source row
        int row = task->row();
        if ( row > 0 )
        {
            // parent index
            QModelIndex pi(parent(idx));
            Task* pt = pi.isValid() ? static_cast<Task*>(pi.internalPointer()) :
                                      root;
            // MOVE
            beginMoveRows(pi, row, row, pi, row-1);
            qSwap(pt->children[row], pt->children[row-1]);
            endMoveRows();
            modified = true;
        }
    }
}

void ProjectModel::moveTaskDown(const QModelIndex& idx)
{
    if ( idx.isValid() )
    {
        // current task
        Task* task = static_cast<Task*>(idx.internalPointer());
        // parent index
        QModelIndex pi(parent(idx));
        // parent task
        Task* pt = pi.isValid() ? static_cast<Task*>(pi.internalPointer()) :
                                  root;
        // source row
        int row = task->row();
        if ( row + 1 < pt->childCount() )
        {
            // MOVE
            beginMoveRows(pi, row, row, pi, row+2);
            qSwap(pt->children[row], pt->children[row+1]);
            endMoveRows();
            modified = true;
        }
    }
}

void ProjectModel::editTask(const QModelIndex &idx, const Task& t)
{
    if ( idx.isValid() ) {
        Task* task = static_cast<Task*>(idx.internalPointer());
        *task = t;
        root->update();
        emit dataChanged(QModelIndex(), QModelIndex());
        modified = true;
    }
}

void ProjectModel::markTaskDone(const QModelIndex& idx)
{
    if ( idx.isValid() )
    {
        // current task
        Task* task = static_cast<Task*>(idx.internalPointer());
        if ( 0 == task->childCount() )
        {
            task->setDone( !task->isDone() );
            root->update();
            emit dataChanged(QModelIndex(),QModelIndex());
            modified = true;
        }
    }
}

void ProjectModel::saveToFile(const QString& fileName)
{
    QFile saveFile(fileName);
    if (saveFile.open(QIODevice::WriteOnly))
    {
        QJsonObject obj;
        root->write(obj);
        QJsonDocument doc(obj);
        saveFile.write(doc.toJson());
        modified = false;
    }
}

bool ProjectModel::loadFromFile(const QString &fileName)
{
    bool result = false;
    QFile loadFile(fileName);
    if (loadFile.open(QIODevice::ReadOnly))
    {
        QByteArray data = loadFile.readAll();
        QJsonDocument doc(QJsonDocument::fromJson(data));
        root->read(doc.object());
        root->update();
        result = true;
    }
    return result;
}
